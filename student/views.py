from django.shortcuts import render
from . models import Student, Book


def index(request):
    
    return render(request, "index.html")
    
def book_list(request):
    books = Book.objects.all()
    return render(request, "book_list.html", {'book_list': books})

def student_list(request):
    students = Student.objects.all()
    context ={'student_list': students}
    return render(request, "student_list.html", context)
