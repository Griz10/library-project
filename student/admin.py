from django.contrib import admin
from .models import Student, Book, Borrow, Payment

admin.site.register(Student)
admin.site.register(Book)
admin.site.register(Borrow)
admin.site.register(Payment)



