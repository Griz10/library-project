from django.db import models


class Student(models.Model):
    first_name = models.CharField(max_length=200,)
    last_name = models.CharField(max_length=200,)
    def __str__(self):
        return self.first_name


class Book(models.Model):
    title = models.CharField(max_length=200,)
    author = models.CharField(max_length=200,)
    book_num = models.FloatField()
    def __str__(self):
        return self.title


class Borrow(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE,related_name="student_id_fk")
    book = models.ForeignKey(Book, on_delete=models.CASCADE,related_name="book_id_fk")
    borrowing_date = models.DateField()
    return_date = models.DateField()

    def __str__(self):
        return self.book.title


class Payment(models.Model):
    amount = models.FloatField()

    def __str__(self):
        return self.amount


    