from django.urls import path
from . import views


urlpatterns=[
    path('', views.index, name='home'),
    path('books', views.book_list, name='books'),
    path('students', views.student_list, name='students'),
    
]